package Engine;

/**Base type for all objects in the game.*/
public abstract class Component {
	
	// Variables
	public boolean Enabled;

	// Constructor
	public Component() {
		Main.MainGame.Components.add(this);
		Created();
	}

	/** Runs when the object is constructed.
	 * Use this instead of constructor when possible.*/
	public void Created() {}

	/** Runs once every frame. */
	public void Update() {}

	/** Runs once every frame. Use this to draw stuff. */
	public void Draw() {}

	/** Same as update, but runs after draw. */
	public void LateUpdate() {}

}
package Engine.Components;

import Engine.*;

public class Player extends Component {
	
	// Variables
	public int Id;
	public String Name;

	// Constructor
	public Player (int id, String name) {
		Id = id;
		Name = name;
	}

}
package Engine;

public class Editor {

	// Variables
	public EditorWindow Window;

	// Constructor
	public Editor() {

		Window = new EditorWindow(this);

	}

	// Starts the game
	public void StartGame(){

		Main.MainGame = new Game();
		Main.MainGame.GameLoop.start();

		// Set UI stuff
		Window.Menu_RunButton.setEnabled(false);
		Window.Menu_StopButton.setEnabled(true);

	}

	// Ends the game
	public void EndGame() {

		Main.MainGame.GameLoop.stop();
		Main.MainGame.Window.dispose();
		Main.MainGame = null;

		// Set UI stuff
		Window.Menu_FrameCounter.setText("");
		Window.Menu_RunButton.setEnabled(true);
		Window.Menu_StopButton.setEnabled(false);

	}

}
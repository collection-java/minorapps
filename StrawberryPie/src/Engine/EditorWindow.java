package Engine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
//import com.apple.eawt.Application;

public class EditorWindow extends JFrame {

    // Variables
    public Editor Editor;

    public JMenuBar Menu;
    public JMenu Menu_File;
    public JMenu Menu_Map;
    public JMenuItem Menu_File_Save;
    public JMenuItem Menu_Map_Size;
    public JButton Menu_RunButton;
    public JButton Menu_StopButton;
    public JLabel Menu_FrameCounter;

    public Canvas Canvas;

    // Constructor
    public EditorWindow(Editor editor) {

        Editor = editor;

        BuildMenu();

        // Set window icon for apple stuff.
        /*Application application = Application.getApplication();
        application.setDockIconImage(Toolkit.getDefaultToolkit().getImage(Game.class.getResource("/icon_strawberrypie.png")));*/

        // Set window icon for others.
        setIconImage(Toolkit.getDefaultToolkit().getImage(Game.class.getResource("/icon_strawberrypie.png")));

        getContentPane().setBackground(new Color(112, 128, 144));
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        setVisible(true);
        setResizable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(720, 480));
        setTitle("Strawberry Pie: Editor");

        Canvas = new Canvas();

    }

    // Builds and adds the menu bar.
    private void BuildMenu() {

        Menu = new JMenuBar();
        setJMenuBar(Menu);

        Menu_File = new JMenu("File");
        Menu.add(Menu_File);

        Menu_File_Save = new JMenuItem("Save");
        Menu_File.add(Menu_File_Save);

        Menu_Map = new JMenu("Map");
        Menu.add(Menu_Map);

        Menu_Map_Size = new JMenuItem("Size..");
        Menu_Map.add(Menu_Map_Size);

        // Button: Run Game
        Menu_RunButton = new JButton("Run Game");
        Menu.add(Menu_RunButton);
        Menu_RunButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                Main.MainEditor.StartGame();

            }

        });

        // Button: Stop Game
        Menu_StopButton = new JButton("Stop Game");
        Menu.add(Menu_StopButton);
        Menu_StopButton.setEnabled(false);
        Menu_StopButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                Main.MainEditor.EndGame();

            }

        });

        Menu_FrameCounter = new JLabel();
        Menu.add(Menu_FrameCounter);

    }

}

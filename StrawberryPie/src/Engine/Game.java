package Engine;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static javax.swing.JFrame.*;

/** This is the game base class. */
public class Game {
	
	// Variables
	public int Frame;
	public Timer GameLoop;
	public ArrayList<Component> Components;
	public GameWindow Window;

	private boolean paused;
	
	// Constructor
	public Game() {

		paused = false;
		Components = new ArrayList<Component>();
		Window = new GameWindow(this);

		GameLoop = new Timer(10, new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				Step();

			}

		});

	}

	// Steps through one frame of the game
	public void Step() {

		if (paused) return;
		for (Component component : Components) {

			component.Update();
			component.Draw();
			component.LateUpdate();

		}
		Frame++;
		Main.MainEditor.Window.Menu_FrameCounter.setText("Loops: " + Integer.toString(Frame));

	}

	// Pauses the game loop
	public void Pause() {

		paused = true;

	}
	
	public void Resume() {

		paused = false;

	}

}
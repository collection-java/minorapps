package Engine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GameWindow extends JFrame {

    // Variables
    public Game Game;
    public Canvas Canvas;

    // Constructor
    public GameWindow(Game game) {

        Game = game;

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        getContentPane().setBackground(new Color(112, 128, 144));
        setIconImage(Toolkit.getDefaultToolkit().getImage("icon_strawberrypie.png"));
        setVisible(true);
        setResizable(false);
        setSize(new Dimension(720, 480));
        setTitle("Strawberry Pie: Game");

        // End game when closing window.
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {

                Main.MainEditor.EndGame();

            }

        });

        Canvas = new Canvas();

    }

}

package Engine;

import java.awt.*;

public class Sprite {
	
	// Variables
	public Image Image;
	public int Width;
	public int Height;
	
	// Constructor
	public Sprite(Image image, int width, int height) {
		Image = image;
		Width = width;
		Height = height;
	}

}
